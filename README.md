## tcpinfo

Programmatic netstat like functionality for golang

```
go get gitlab.com/mjgarton/tcpinfo
```

Example:

```
import (
	"encoding/json"
	"fmt"
	"gitlab.com/mjgarton/tcpinfo"
	"log"
)

func main() {
	info, err := tcpinfo.ParseProcNetTCP()
	if err != nil {
		log.Fatal(err)
	}
	j, err := json.MarshalIndent(info, "", "  ")
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("%s\n", j)
}
```
