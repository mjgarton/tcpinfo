package tcpinfo

import (
	"bufio"
	"encoding/binary"
	"errors"
	"fmt"
	"io/ioutil"
	"net"
	"strings"
)

type TCPConnectionInfo struct {
	LocalAddr  net.IP
	RemoteAddr net.IP
	LocalPort  int
	RemotePort int
	State      TCPState
	//TransmitQueue int
	//ReceiveQueue  int
	// TODO: timers
	//RTOTimeouts int
	// TODO? uid
	//RTO int
}

type TCPState int

func (t TCPState) String() string {
	switch t {
	case StateEstablished:
		return "ESTABLISHED"
	case StateSynSent:
		return "SYN_SENT"
	case StateSynRecv:
		return "SYN_RECV"
	case StateFinWait1:
		return "FIN_WAIT1"
	case StateFinWait2:
		return "FIN_WAIT2"
	case StateTimeWait:
		return "TIME_WAIT"
	case StateCloseWait:
		return "CLOSE_WAIT"
	case StateLastAck:
		return "LAST_ACK"
	case StateListen:
		return "LISTEN"
	default:
		panic(fmt.Sprintf("bug: unimplemented state: %d", t))
	}
}

func (t *TCPState) MarshalText() (text []byte, err error) {
	return []byte(t.String()), nil
}

const (
	StateEstablished = TCPState(0x01)
	StateSynSent     = TCPState(0x02)
	StateSynRecv     = TCPState(0x03)
	StateFinWait1    = TCPState(0x04)
	StateFinWait2    = TCPState(0x05)
	StateTimeWait    = TCPState(0x06)
	StateCloseWait   = TCPState(0x08)
	StateLastAck     = TCPState(0x09)
	StateListen      = TCPState(0x0a)
)

// ParseProcNetTCPSafe will retry if it finds connections duplicated with different states. I have no idea why that happens in the first place.
func ParseProcNetTCPSafe() ([]TCPConnectionInfo, error) {
	for tries := 0; tries < 16; tries++ {
		parsed, err := ParseProcNetTCP()
		if err != nil || !dupes(parsed) {
			return parsed, err
		}
	}
	return nil, errors.New("failed to read tcp connection info without duplicates")
}

func dupes(conninfo []TCPConnectionInfo) bool {
	for i, a := range conninfo {
		for _, b := range conninfo[i+1:] {
			if a.LocalAddr.Equal(b.LocalAddr) &&
				a.LocalPort == b.LocalPort &&
				a.RemoteAddr.Equal(b.RemoteAddr) &&
				a.RemotePort == b.RemotePort {
				return true
			}
		}
	}
	return false

}

func ParseProcNetTCP() ([]TCPConnectionInfo, error) {
	var info []TCPConnectionInfo
	if err := parseProcNetTCP("/proc/net/tcp", &info); err != nil {
		return nil, err
	}
	if err := parseProcNetTCP("/proc/net/tcp6", &info); err != nil {
		return nil, err
	}
	return info, nil
}

func parseProcNetTCP(filename string, info *[]TCPConnectionInfo) error {
	data, err := ioutil.ReadFile(filename)
	if err != nil {
		return err
	}
	return doParse(string(data), info)
}

func doParse(input string, ci *[]TCPConnectionInfo) error {

	s := bufio.NewScanner(strings.NewReader(input))

	s.Scan() // skip line of headings
	for s.Scan() {
		str := s.Text()
		ps := bufio.NewScanner(strings.NewReader(str))
		ps.Split(bufio.ScanWords)
		var parts []string
		for ps.Scan() {
			parts = append(parts, ps.Text())
		}
		//fmt.Printf("%v len:%d\n", parts, len(parts))
		if len(parts) != 17 && len(parts) != 12 {
			return fmt.Errorf("failed to parse line: %v\n", parts)
		}

		info := TCPConnectionInfo{}

		var err error
		info.LocalAddr, info.LocalPort, err = toIPAndPort(parts[1])
		if err != nil {
			return err
		}
		info.RemoteAddr, info.RemotePort, err = toIPAndPort(parts[2])
		if err != nil {
			return err
		}
		if _, err := fmt.Sscanf(parts[3], "%02x", &info.State); err != nil {
			return err
		}

		*ci = append(*ci, info)
	}

	return nil
}

func toIPAndPort(input string) (net.IP, int, error) {

	hostPort := strings.Split(input, ":")
	if len(hostPort) != 2 {
		return net.IP{}, 0, fmt.Errorf("failed to parse host and port: %s", input)
	}
	addr, err := toIP(hostPort[0])
	if err != nil {
		return net.IP{}, 0, fmt.Errorf("failed to parse host and port: %s", input)
	}
	port, err := toPort(hostPort[1])
	if err != nil {
		return net.IP{}, 0, fmt.Errorf("failed to parse host and port: %s", input)
	}
	return addr, port, nil
}

func toIP(hexstring string) (net.IP, error) {
	var data []byte
	a := make([]uint32, len(hexstring)/8)

	var err error
	switch len(hexstring) {
	case 8: //ipv4
		_, err = fmt.Sscanf(hexstring, "%08x", &a[0])
	case 32: //ipv6
		_, err = fmt.Sscanf(hexstring, "%08x%08x%08x%08x", &a[0], &a[1], &a[2], &a[3])
	default:
		err = fmt.Errorf("can't parse IP address from %v\n", hexstring)
	}

	if err != nil {
		return nil, err
	}

	data = make([]byte, len(a)*4)
	for i := 0; i < len(data); i += 4 {
		binary.BigEndian.PutUint32(data[i:], a[i/4])
	}

	for i := 0; i < len(data); i += 4 {
		rev(data[i : i+4])
	}
	return net.IP(data), nil
}

func rev(data []byte) {
	l := len(data)
	for i := 0; i < l/2; i++ {
		data[i], data[l-i-1] = data[l-i-1], data[i]
	}
}

func toPort(hexstring string) (int, error) {
	var port int
	_, err := fmt.Sscanf(hexstring, "%04x", &port)
	return port, err
}
