package tcpinfo

import (
	"bytes"
	"testing"
)

func TestToIP4(t *testing.T) {
	ip, err := toIP("0100007F")
	if err != nil || ip.String() != "127.0.0.1" {
		t.Fatal("fail")
	}
}

func TestToIP6(t *testing.T) {
	ip, err := toIP("5014002A010C13400000000071000000")
	expected := "2a00:1450:4013:c01::71"
	if err != nil || ip.String() != expected {
		t.Fatalf("expected %v but got %v", expected, ip.String())
	}
}

func TestToPort0(t *testing.T) {
	p, err := toPort("0000")
	if err != nil || p != 0 {
		t.Fatal("fail")
	}
}

func TestToPort(t *testing.T) {
	p, err := toPort("86A8")
	if err != nil || p != 34472 {
		t.Fatalf("expected %d but got %d for input %s", 34472, p, "86A8")
	}
}

func TestRev(t *testing.T) {

	data := []struct {
		in       []byte
		expected []byte
	}{
		{[]byte{}, []byte{}},
		{[]byte{1, 2, 3}, []byte{3, 2, 1}},
		{[]byte{1, 2, 3, 4}, []byte{4, 3, 2, 1}},
	}

	for _, dat := range data {
		out := make([]byte, len(dat.in))
		copy(out, dat.in)
		rev(out)
		if !bytes.Equal(out, dat.expected) {
			t.Fatalf("expected %v but got %v\n", dat.expected, out)
		}
	}
}
